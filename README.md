# Ghost Leg

A starter project for exploring Kotlin solutions to the Ghost Leg Kata.

See David Sturgis' [Google doc description](https://docs.google.com/document/d/1BH8zmgV-SA7RSd6H5V6SoZ8sl4jQ0IBxmibgN9UWFPM)

See the [Wikipedia article](https://en.wikipedia.org/wiki/Ghost_Leg) as well, particularly the graphic labeled "Amidakuji" on the right side of the first screen.
